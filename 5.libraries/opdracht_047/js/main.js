var actors = [
    {
        name: 'Harrison Ford',
        birthday: '13-7-1942',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTY4Mjg0NjIxOV5BMl5BanBnXkFtZTcwMTM2NTI3MQ@@._V1_UX214_CR0,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'Raiders of the lost ark',
                year: '1981'
            }
        ]
    },{
        name: 'Michelle Pfeiffer',
        birthday: '29-04-1956',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTUzNjI0Njc5NF5BMl5BanBnXkFtZTYwOTM2MjYz._V1_UX214_CR0,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'Dangerous Minds',
                year: '1995'
            },
            {
                name: 'The Fabulous Baker Boys',
                year: '1989'
            }
        ]
    },{
        name: 'Arnold Schwarzenegger',
        birthday: '30-7-1947',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTI3MDc4NzUyMV5BMl5BanBnXkFtZTcwMTQyMTc5MQ@@._V1_UY317_CR19,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'Total Recall',
                year: '1990'
            }
        ]
    },{
        name: 'Meg Ryan',
        birthday: '19-11-1961',
        pictures: 'https://ia.media-imdb.com/images/M/MV5BMTgzOTI0NzI3OV5BMl5BanBnXkFtZTgwODIyMzUzMDI@._V1_UY317_CR1,0,214,317_AL_.jpg',
        movies: [
            {
                name: 'When Harry Met Sally',
                year: '1989'
            },
            {
                name: 'Sleepless In Seattle',
                year: '1993'
            }
        ]
    }
];

function update() {
    var $container = $('#main-container');
    $container.empty();
    for (var i=0; i<actors.length; i++) {
        var actor = actors[i];

        var $name = $('<div class="name"></div>');
        $name.html(actor.name);

        var $birthDay = $('<div class="birthday"></div>');
        $birthDay.html(actor.birthday);

        var $img = $('<img class="image">');
        $img.attr("src",actor.pictures);

        var $imgContainer = $('<div class="image-container"></div>');
        $imgContainer.append($img);

        var $movieContainer = $('<div class="movie-container"></div>');
        for (var j=0; j<actor.movies.length; j++) {
            var movie = actor.movies[j];
            var $movie = $('<div class="movie"></div>');
            $movie.html(movie.name + ' (' + movie.year + ')');
            $movieContainer.append($movie);
        }

        var $delete = $('<div class="delete">x</div>');
        $delete.data('id',i);
        $delete.hide();
        $delete.click(function() {
            actors.splice($(this).data('id'),1);
            update();
        });


        var $actorContainer = $('<div class="actor-container"></div>');
        $actorContainer.append($name).append($birthDay).append($imgContainer).append($movieContainer).append($delete);
        $container.append($actorContainer);

        $actorContainer.mouseover(function() {
            $(this).find('.delete').show();
        });
        $actorContainer.mouseout(function() {
            $(this).find('.delete').hide();
        });
    }

}
update();